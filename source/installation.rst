How to install
================

Users:

Simply do

.. code-block:: python

	pip install futuraAiToolbox


For developers

Developers might want to take a git copy for the bleeding edge code.

* clone the repo at <repo id>
* `python setup.py install`
* test the example code at examples folder

