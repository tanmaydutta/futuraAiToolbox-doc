futuraAiToolbox package
=======================

Subpackages
-----------

.. toctree::

    futuraAiToolbox.guiBackends
    futuraAiToolbox.quant

Submodules
----------

futuraAiToolbox.futuraAiToolbox module
--------------------------------------

.. automodule:: futuraAiToolbox.futuraAiToolbox
    :members:
    :undoc-members:
    :show-inheritance:

futuraAiToolbox.futuraAiUtils module
------------------------------------

.. automodule:: futuraAiToolbox.futuraAiUtils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: futuraAiToolbox
    :members:
    :undoc-members:
    :show-inheritance:
