.. futuraAiToolbox documentation master file, created by
   sphinx-quickstart on Tue Apr 17 10:32:51 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to futuraAiToolbox's documentation!
===========================================

To get started quickly, read the trading system structure.
For deep dive browse the module. Happy coding!!



Table of contents:
===========================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   installation
   packages
   trading_system
   market_position_trading
   settings
   evaluate_system
   performance
   optimization
   ..
      submit_system
      reference
.. toctree::
   :maxdepth: 2
   :caption: Contents:



Index for source code(*for advanced users*)
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Contact Us
=============

Questions, comments, concerns or just want to say hello?

Contact us at tanmay@futuraai.com

Visit us at `FuturaAi`_

.. _`FuturaAi`: https://www.futura.ai
